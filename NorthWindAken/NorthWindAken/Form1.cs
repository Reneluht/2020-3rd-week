﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NorthWindAken
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Tooted_Click(object sender, EventArgs e)
        {
            using(NorthwindEntities db = new NorthwindEntities())
            {
                var products = db.Products
                    .Select(x => new { x.ProductID, x.ProductName, x.UnitPrice })
                    .ToList();
                this.dataGridView1.DataSource = products;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Kategooriad_Click(object sender, EventArgs e)
        {
            using (NorthwindEntities db = new NorthwindEntities())
            {
                var categories = db.Categories
                    .Select( x => new { x.CategoryID, x.CategoryName, x.Description, Tooted = x.Products.Count() })
                    .ToList();
                this.dataGridView1.DataSource = categories;
            }
        }
    }
}
