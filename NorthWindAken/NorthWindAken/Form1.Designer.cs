﻿namespace NorthWindAken
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Tooted = new System.Windows.Forms.Button();
            this.Kategooriad = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(44, 34);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.Size = new System.Drawing.Size(809, 584);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Tooted
            // 
            this.Tooted.Location = new System.Drawing.Point(1008, 420);
            this.Tooted.Name = "Tooted";
            this.Tooted.Size = new System.Drawing.Size(130, 73);
            this.Tooted.TabIndex = 1;
            this.Tooted.Text = "Tooted";
            this.Tooted.UseVisualStyleBackColor = true;
            this.Tooted.Click += new System.EventHandler(this.Tooted_Click);
            // 
            // Kategooriad
            // 
            this.Kategooriad.Location = new System.Drawing.Point(1008, 256);
            this.Kategooriad.Name = "Kategooriad";
            this.Kategooriad.Size = new System.Drawing.Size(130, 71);
            this.Kategooriad.TabIndex = 2;
            this.Kategooriad.Text = "Kategooriad";
            this.Kategooriad.UseVisualStyleBackColor = true;
            this.Kategooriad.Click += new System.EventHandler(this.Kategooriad_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1341, 688);
            this.Controls.Add(this.Kategooriad);
            this.Controls.Add(this.Tooted);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button Tooted;
        private System.Windows.Forms.Button Kategooriad;
    }
}

