﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dünaamikud
{
    class Bag : DynamicObject
    {
        dynamic bag;
        public Bag()
        {
            bag = new Dictionary<string, dynamic>();
        }
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = bag.ContainsKey(binder.Name) ? bag[binder.Name] : null;
            return true;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int arv = 7;
            string sõna = "Miskisõna";

            object o = arv;
            Console.WriteLine(o);
            o = sõna;
            Console.WriteLine(o);

            dynamic d = arv;
            d++;
            Console.WriteLine(d);
            d = sõna;
            Console.WriteLine(d.ToLower());

            dynamic bbb = new Bag();
            bbb.Nimi = " RENE ";
            bbb.Vanus = 64;
            bbb.Vanus++;
            Console.WriteLine(bbb);
        }
    }
}
