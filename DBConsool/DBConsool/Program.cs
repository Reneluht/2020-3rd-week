﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DBConsool
{
    class Program
    {
        static void Main(string[] args)
        {

            // Connection - minu ja DB vaheline ühendus
            string connectionString = "Data Source=valiitsql.database.windows.net;" +
                "Initial Catalog=Northwind;" +
                "Persist Security Info=True;" +
                "User ID=Student;" +
                "Password=Pa$$w0rd";
            var conn = new SqlConnection(connectionString);
            conn.Open(); // CONNECT
            // Command - Minu küsimus DB
                         
            string küsimus = "select productname, unitprice from products"; // COMMAND (küsimus)
            // Datareader - tema vastus minu küsimusele
            var comm = new SqlCommand(küsimus, conn);
            var R = comm.ExecuteReader();
            while (R.Read())
            {
                Console.WriteLine($"{R["productname"]} hinnaga {R["unitprice"]}");
            }
        }
    }
}
