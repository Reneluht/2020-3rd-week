﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities db = new NorthwindEntities();
            // KARUKÕHT
            // db.Database.Log = Console.WriteLine; 

            // Siin on päring Extensionina
            var q1 = db.Products
                .Where(x => x.UnitPrice < 10)
                .OrderBy(x => x.UnitPrice)
                .Select(x => new { x.ProductName, x.UnitPrice, x.UnitsInStock });
            foreach (var x in q1)
            {
                Console.WriteLine(x);
            }

            // Siin on päring LINQ avaldisena
            var q2 = from x in db.Products
                     where x.UnitPrice < 10
                     orderby x.UnitPrice
                     select new { x.ProductName, x.UnitPrice, x.UnitsInStock };

            var kala = db.Categories.Find(8);
            Console.WriteLine(kala.CategoryName);

            kala.CategoryName = "Seatoit";
            db.SaveChanges();

            //foreach (var x in db.Products)
            //{
            //    Console.WriteLine(x.ProductName);
            //}
        }
    }
}
