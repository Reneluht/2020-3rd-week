﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Õpilased.Models;

namespace Õpilased.Controllers
{
    public class ÕpilaneController : Controller
    {
        // GET: Õpilane
        public ActionResult Index()
        {
            return View(Õpilane.Õpilased);
        }

        // GET: Õpilane/Details/5
        public ActionResult Details(string id)
        {
            Õpilane õ = Õpilane.Find(id);
            if (õ == null)
                return HttpNotFound();
            return View(õ);
        }

        // GET: Õpilane/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Õpilane/Create
        [HttpPost]
        public ActionResult Create(Õpilane õpilane)
        {

            // TODO: Add insert logic here
            if (õpilane.Add())
                return RedirectToAction("Index");

            else
            {
                ViewBag.Error = "selline isikood juba eksisteerib";
                return View(õpilane);
            }
        }

        // GET: Õpilane/Edit/5
        public ActionResult Edit(string id)
        {
            Õpilane õ = Õpilane.Find(id);
            if (õ == null) return HttpNotFound();
            return View(õ);
        }

        // POST: Õpilane/Edit/5
        [HttpPost]
        public ActionResult Edit(string id, Õpilane õpilane)
        {
            Õpilane õ = Õpilane.Find(id);
            try
            {
                // TODO: Add update logic here
                if (õ != null)
                {
                    õ.Nimi = õpilane.Nimi;
                    õ.Klass = õpilane.Klass;
                }
                  
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Õpilane/Delete/5
        public ActionResult Delete(string id)
        {
            return View();
        }

        // POST: Õpilane/Delete/5
        [HttpPost]
        public ActionResult Delete(string id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                Õpilane.Find(id)?.Delete();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(Õpilane.Find(id));
            }
        }
    }
}
