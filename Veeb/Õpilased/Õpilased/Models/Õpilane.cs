﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Õpilased.Models
{
    public class Õpilane
    {
        // PROPERTY'd
        [Key]
        [Display(Name = "Isikukood")]
        public string IK { get; set; }
        public string Nimi { get; set; }
        public string Klass { get; set; }
        private static Dictionary<string, Õpilane> _Õpilased = new Dictionary<string, Õpilane>(); // see on dictionary, kus hakkan õpilaisi hoidma
        public static IEnumerable<Õpilane> Õpilased => _Õpilased.Values; // see on avalik "List", mis sisaldab õpilaisi

        // Add funktsioon lisab õpilase dictionary, kui teda seal ei ole
        public bool Add()
        {
            if (!_Õpilased.ContainsKey(this.IK))
            {
                _Õpilased.Add(IK, this);
                return true;
            }
            else return false;
        }

        // FUNKTSIOONID
        // find funktsioon leiab õpilase dictionarist
        public static Õpilane Find(string ik)
            => _Õpilased.ContainsKey(ik) ? _Õpilased[ik] : null;

        // delete meetod kustutab ta dictionarist
        public void Delete()
        {
            if (_Õpilased.ContainsKey(this.IK)) _Õpilased.Remove(this.IK);
        }
    }
}