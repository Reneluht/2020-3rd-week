﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AutentimineMVC.Startup))]
namespace AutentimineMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
