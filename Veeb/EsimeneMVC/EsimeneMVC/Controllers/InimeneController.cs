﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EsimeneMVC.Models;

namespace EsimeneMVC.Controllers
{

    // C - CREATE
    // R - READ
    // U - UPDATE
    // D - DELETE
    // L - LIST
    public class InimeneController : Controller
    {
        // GET: Inimene
        public ActionResult Index()  // LIST
        {
            return View(Inimene.Inimesed);
        }

        // GET: Inimene/Details/5
        public ActionResult Details(int id)  // READ
        {
            Inimene x = Inimene.Find(id);
            if (x == null)
                return HttpNotFound();
            return View(x);
        }

        // GET: Inimene/Create
        public ActionResult Create()    // CREATE
        {
            return View();
        }

        // POST: Inimene/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                Inimene.Inimesed.Add(
                    new Inimene
                    {
                        Nimi = collection["Nimi"],
                        Vanus = int.Parse(collection["Vanus"])
                    })
                    ;
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Edit/5
        public ActionResult Edit(int id)  // UPDATE
        {
            Inimene x = Inimene.Find(id);
            if (x == null)
                return HttpNotFound();
            return View(x);
        }

        // POST: Inimene/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Inimene inimene)
        {
            try
            {
                // TODO: Add update logic here
                    var x = Inimene.Find(id);
                    x.Nimi = inimene.Nimi;
                    x.Vanus = inimene.Vanus;
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Delete/5
        public ActionResult Delete(int id)  // DELETE
        {
            Inimene x = Inimene.Find(id);
            if (x == null)
                return HttpNotFound();
            return View(x);
        }

        // POST: Inimene/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                Inimene x = Inimene.Find(id);
                if (x != null)
                    Inimene.Inimesed.Remove(x);
                
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
