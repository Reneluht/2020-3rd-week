﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AknaÜlesanne
{
    public partial class Form1 : Form
    {
        NorthwindEntities db = new NorthwindEntities();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listBox1.DataSource =
                db.Categories
                .OrderBy(x => x.CategoryID)
                .Select(x => x.CategoryName)
                .ToList();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Text = (checkBox1.Checked ?
                "Näitan kõiki tooteid" :
                "Näitan ainult kehtivaid tooteid")
                + " kategooriast " + listBox1.SelectedItem
                ;

            var tooted = db.Products
                .Where(x => this.checkBox1.Checked || !x.Discontinued)
                .Where(x => x.Category.CategoryName == this.listBox1.SelectedItem.ToString())
                .Select(x => new { x.ProductID, x.ProductName, x.UnitPrice, x.Discontinued })
               .ToList();
            this.dataGridView1.DataSource = tooted;

        }

        private void Form1_Load_1(object sender, EventArgs e)
        {

        }
    }
}
