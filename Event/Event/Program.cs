﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event
{
    class Program
    {
        static void Juubelipidu(Inimene i)
        {
            Console.WriteLine("Teeme pidu!");
        }
        static void Main(string[] args)
        {
            Inimene rene = new Inimene { Nimi = "Rene", Vanus = 28};
            rene.Juubel += x => Console.WriteLine($"Nüüd saab pidu - {x.Nimi} saab {x.Vanus} aastaseks");
            rene.PensioniIga += x => Console.WriteLine("kas pensionile ei peaks minema?");

            rene.Juubel += Juubelipidu;
            rene.Juubel -= Juubelipidu;

            while (rene.Vanus < 120)
            {
                rene.Vanus++;
            }
            
        }
    }
    class Inimene
    {
        public event Action<Inimene> PensioniIga;
        public event Action<Inimene> Juubel;
        public string Nimi;
        private int vanus = 0;
        bool kasOnPensionil = false;
        public void Pensionile() => this.kasOnPensionil = true;
        public int Vanus { get => vanus;
            set
            {

                if (value < vanus) throw new Exception("Inimest nooremaks teha ei saa");
                vanus = value;
                if (vanus % 25 == 0)
                    if (Juubel != null) this.Juubel(this);
                if (vanus > 70) PensioniIga?.Invoke(this);
                if (vanus > 70 && !kasOnPensionil) PensioniIga?.Invoke(this);
            }
        }
    }
}
