﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulaator
{
    public partial class Form1 : Form
    {
        string tehe = "";
        TextBox kumb = null;


        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            kumb = textBox1;

        }

        private void Number_Click(object sender, EventArgs e)
        {
            kumb.Text += ((Button)sender).Text;
            label1.Text = "";
        }

        private void Tehe_Click(object sender, EventArgs e)
        {
            tehe = ((Button)sender).Text;
            kumb = textBox2;
            label1.Text = "";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Clear_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            label1.Text = "";
            kumb = textBox1;
        }

        private void kalkuleeri_Click(object sender, EventArgs e)
        {
            try
            {
                int arv1 = int.Parse(textBox1.Text);
                int arv2 = int.Parse(textBox2.Text);
                int result =
                    tehe == "+" ? arv1 + arv2 :
                    tehe == "-" ? arv1 - arv2 :
                    tehe == "*" ? arv1 * arv2 :
                    tehe == "/" ? arv1 / arv2 : 0;
                label1.Text = result.ToString();
            }
            catch (Exception err)
            {
                label1.Text = err.Message;
            }
        }


    }
}
