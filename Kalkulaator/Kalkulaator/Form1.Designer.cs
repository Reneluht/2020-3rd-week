﻿namespace Kalkulaator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.liida = new System.Windows.Forms.Button();
            this.lahuta = new System.Windows.Forms.Button();
            this.jaga = new System.Windows.Forms.Button();
            this.korruta = new System.Windows.Forms.Button();
            this.kalkuleeri = new System.Windows.Forms.Button();
            this.Clear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 44);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.textBox2.Location = new System.Drawing.Point(139, 12);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(119, 44);
            this.textBox2.TabIndex = 1;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(12, 94);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 68);
            this.button1.TabIndex = 2;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Number_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.button2.Location = new System.Drawing.Point(98, 94);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(77, 68);
            this.button2.TabIndex = 3;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Number_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.button3.Location = new System.Drawing.Point(181, 94);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(77, 68);
            this.button3.TabIndex = 4;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Number_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.button4.Location = new System.Drawing.Point(12, 168);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 68);
            this.button4.TabIndex = 5;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Number_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.button5.Location = new System.Drawing.Point(98, 168);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(80, 68);
            this.button5.TabIndex = 6;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Number_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.button6.Location = new System.Drawing.Point(184, 168);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(74, 68);
            this.button6.TabIndex = 7;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Number_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.button7.Location = new System.Drawing.Point(12, 242);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(80, 68);
            this.button7.TabIndex = 8;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Number_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.button8.Location = new System.Drawing.Point(98, 242);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(77, 68);
            this.button8.TabIndex = 9;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Number_Click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.button9.Location = new System.Drawing.Point(184, 242);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(74, 68);
            this.button9.TabIndex = 10;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.Number_Click);
            // 
            // button0
            // 
            this.button0.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.button0.Location = new System.Drawing.Point(98, 316);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(77, 68);
            this.button0.TabIndex = 11;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.Number_Click);
            // 
            // liida
            // 
            this.liida.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold);
            this.liida.Location = new System.Drawing.Point(330, 94);
            this.liida.Name = "liida";
            this.liida.Size = new System.Drawing.Size(77, 68);
            this.liida.TabIndex = 12;
            this.liida.Text = "+";
            this.liida.UseVisualStyleBackColor = true;
            this.liida.Click += new System.EventHandler(this.Tehe_Click);
            // 
            // lahuta
            // 
            this.lahuta.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lahuta.Location = new System.Drawing.Point(330, 168);
            this.lahuta.Name = "lahuta";
            this.lahuta.Size = new System.Drawing.Size(77, 68);
            this.lahuta.TabIndex = 13;
            this.lahuta.Text = "-";
            this.lahuta.UseVisualStyleBackColor = true;
            this.lahuta.Click += new System.EventHandler(this.Tehe_Click);
            // 
            // jaga
            // 
            this.jaga.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.jaga.Location = new System.Drawing.Point(330, 316);
            this.jaga.Name = "jaga";
            this.jaga.Size = new System.Drawing.Size(77, 68);
            this.jaga.TabIndex = 14;
            this.jaga.Text = "/";
            this.jaga.UseVisualStyleBackColor = true;
            this.jaga.Click += new System.EventHandler(this.Tehe_Click);
            // 
            // korruta
            // 
            this.korruta.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.korruta.Location = new System.Drawing.Point(330, 242);
            this.korruta.Name = "korruta";
            this.korruta.Size = new System.Drawing.Size(77, 68);
            this.korruta.TabIndex = 15;
            this.korruta.Text = "*";
            this.korruta.UseVisualStyleBackColor = true;
            this.korruta.Click += new System.EventHandler(this.Tehe_Click);
            // 
            // kalkuleeri
            // 
            this.kalkuleeri.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.kalkuleeri.Location = new System.Drawing.Point(184, 316);
            this.kalkuleeri.Name = "kalkuleeri";
            this.kalkuleeri.Size = new System.Drawing.Size(74, 68);
            this.kalkuleeri.TabIndex = 16;
            this.kalkuleeri.Text = "=";
            this.kalkuleeri.UseVisualStyleBackColor = true;
            this.kalkuleeri.Click += new System.EventHandler(this.kalkuleeri_Click);
            // 
            // Clear
            // 
            this.Clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Clear.Location = new System.Drawing.Point(12, 316);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(80, 68);
            this.Clear.TabIndex = 17;
            this.Clear.Text = "C";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(323, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 37);
            this.label1.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 396);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.kalkuleeri);
            this.Controls.Add(this.korruta);
            this.Controls.Add(this.jaga);
            this.Controls.Add(this.lahuta);
            this.Controls.Add(this.liida);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button liida;
        private System.Windows.Forms.Button lahuta;
        private System.Windows.Forms.Button jaga;
        private System.Windows.Forms.Button korruta;
        private System.Windows.Forms.Button kalkuleeri;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Label label1;
    }
}

