﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AknaÜL2
{
    public partial class Form1 : Form
    {
        List<Product> products = new List<Product>();
        string sorting = "";

        NorthwindEntities db = new NorthwindEntities();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            dataGridView1.DataSource =
                db.Products
                .OrderBy(x => x.ProductName)
                .Select(x => new { x.ProductName, x.UnitPrice })
                .ToList();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var products = db.Products
                .Where(x => nimeOsa.Text == "" || x.ProductName.Contains(nimeOsa.Text))
                .ToList();
            this.dataGridView1.DataSource = products;

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
