﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ÜlesanneAKEN
{
    public partial class Form1 : Form
    {
        List<Inimene> Loetud = null;
        string fileName = "";
        bool salv = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Õpilased_Click(object sender, EventArgs e)
        {
            salv = false;
            this.fileName = @"..\..\Õpilased.txt";
            this.Loetud = File.ReadAllLines(fileName)
                .Select(x => x.Replace(", ", ",").Split(','))
                .Select(x => new Inimene { IK = x[0], Nimi = x[1], Klass = x[2] })
                .ToList();
            this.dataGridView1.Visible = true;
            this.dataGridView1.DataSource = Loetud;
            this.dataGridView1.Columns["Aine"].Visible = false;
            this.Salvesta.Visible = true;

        }

        private void Õpetajad_Click(object sender, EventArgs e)
        {
            salv = true;
            this.fileName = @"..\..\Õpetajad.txt";
            this.Loetud = File.ReadAllLines(fileName)
                .Select(x => (x + ",,").Replace(", ", ",").Split(','))
                .Select(x => new Inimene { IK = x[0], Nimi = x[1], Klass = x[3], Aine = x[2] })
                .ToList();
            this.dataGridView1.Visible = true;
            this.dataGridView1.DataSource = Loetud;
            this.dataGridView1.Columns["Aine"].Visible = true;
            this.Salvesta.Visible = true;

        }

        private void Salvesta_Click(object sender, EventArgs e)
        {
            File.WriteAllLines(fileName,
               Loetud
               .Select(x => salv ?
                       $"{x.IK},{x.Nimi},{x.Aine},{x.Klass}" :   // õpetajarida
                       $"{x.IK},{x.Nimi},{x.Klass}"     // õpilasterida
               ))
               ;

        }
    }
}
