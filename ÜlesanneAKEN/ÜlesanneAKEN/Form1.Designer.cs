﻿namespace ÜlesanneAKEN
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Õpilased = new System.Windows.Forms.Button();
            this.Õpetajad = new System.Windows.Forms.Button();
            this.Salvesta = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Õpilased
            // 
            this.Õpilased.Location = new System.Drawing.Point(164, 41);
            this.Õpilased.Name = "Õpilased";
            this.Õpilased.Size = new System.Drawing.Size(109, 47);
            this.Õpilased.TabIndex = 0;
            this.Õpilased.Text = "Õpilased";
            this.Õpilased.UseVisualStyleBackColor = true;
            this.Õpilased.Click += new System.EventHandler(this.Õpilased_Click);
            // 
            // Õpetajad
            // 
            this.Õpetajad.Location = new System.Drawing.Point(164, 147);
            this.Õpetajad.Name = "Õpetajad";
            this.Õpetajad.Size = new System.Drawing.Size(109, 48);
            this.Õpetajad.TabIndex = 1;
            this.Õpetajad.Text = "Õpetajad";
            this.Õpetajad.UseVisualStyleBackColor = true;
            this.Õpetajad.Click += new System.EventHandler(this.Õpetajad_Click);
            // 
            // Salvesta
            // 
            this.Salvesta.Location = new System.Drawing.Point(922, 95);
            this.Salvesta.Name = "Salvesta";
            this.Salvesta.Size = new System.Drawing.Size(109, 39);
            this.Salvesta.TabIndex = 2;
            this.Salvesta.Text = "Salvesta";
            this.Salvesta.UseVisualStyleBackColor = true;
            this.Salvesta.Click += new System.EventHandler(this.Salvesta_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(187, 251);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(637, 441);
            this.dataGridView1.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1792, 831);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.Salvesta);
            this.Controls.Add(this.Õpetajad);
            this.Controls.Add(this.Õpilased);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Õpilased;
        private System.Windows.Forms.Button Õpetajad;
        private System.Windows.Forms.Button Salvesta;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}

